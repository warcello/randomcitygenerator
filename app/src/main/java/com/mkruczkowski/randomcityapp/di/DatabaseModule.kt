package com.mkruczkowski.randomcityapp.di

import android.content.Context
import androidx.room.Room.databaseBuilder
import com.mkruczkowski.randomcityapp.infrastructure.db.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    fun provideCityDao(appDatabase: AppDatabase) = appDatabase.cityDao()

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context) =
        databaseBuilder(context, AppDatabase::class.java, "AppDatabase").build()

}