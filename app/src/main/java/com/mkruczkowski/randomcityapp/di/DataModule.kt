package com.mkruczkowski.randomcityapp.di

import com.mkruczkowski.randomcityapp.domain.CityRepository
import com.mkruczkowski.randomcityapp.domain.ForegroundTask
import com.mkruczkowski.randomcityapp.domain.RandomCityGenerator
import com.mkruczkowski.randomcityapp.domain.TimestampProvider
import com.mkruczkowski.randomcityapp.infrastructure.CityRepositoryImpl
import com.mkruczkowski.randomcityapp.infrastructure.TimestampProviderImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataModule {

    @Binds
    abstract fun bindTimestampProvider(impl: TimestampProviderImpl): TimestampProvider

    @Binds
    @Singleton
    abstract fun bindCityRepository(impl: CityRepositoryImpl): CityRepository

    @Binds
    abstract fun bindCityGenerator(impl: RandomCityGenerator): ForegroundTask

}