package com.mkruczkowski.randomcityapp.infrastructure

import android.app.Application
import androidx.lifecycle.ProcessLifecycleOwner
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class RandomCityApplication : Application() {

    @Inject
    lateinit var foregroundObserver: ForegroundObserver

    override fun onCreate() {
        super.onCreate()
        ProcessLifecycleOwner.get().lifecycle.addObserver(foregroundObserver)
    }

}