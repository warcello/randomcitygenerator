package com.mkruczkowski.randomcityapp.infrastructure.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface CityDao {

    @Insert
    suspend fun insertCity(city: CityEntity)

    @Query("""SELECT * FROM City""")
    fun getCities(): Flow<List<CityEntity>>

}