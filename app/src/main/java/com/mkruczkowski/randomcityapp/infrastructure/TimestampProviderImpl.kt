package com.mkruczkowski.randomcityapp.infrastructure

import com.mkruczkowski.randomcityapp.domain.TimestampProvider
import java.time.Instant
import java.time.ZoneOffset.UTC
import java.time.format.DateTimeFormatter
import javax.inject.Inject

private const val TIME_FORMAT = "dd-MM-yyyy HH:mm:ss"

class TimestampProviderImpl @Inject constructor() : TimestampProvider {

    override fun getUTCNow(): String = DateTimeFormatter
        .ofPattern(TIME_FORMAT)
        .withZone(UTC)
        .format(Instant.now())

}