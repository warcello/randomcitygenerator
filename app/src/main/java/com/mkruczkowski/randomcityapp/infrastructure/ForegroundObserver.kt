package com.mkruczkowski.randomcityapp.infrastructure

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.mkruczkowski.randomcityapp.domain.ForegroundTask
import javax.inject.Inject

class ForegroundObserver @Inject constructor(private val task: ForegroundTask) : LifecycleObserver {
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() {
        task.start()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onMoveToBackground() {
        task.stop()
    }
}