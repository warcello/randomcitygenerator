package com.mkruczkowski.randomcityapp.infrastructure

import com.mkruczkowski.randomcityapp.domain.City
import com.mkruczkowski.randomcityapp.domain.CityRepository
import com.mkruczkowski.randomcityapp.infrastructure.db.CityDao
import com.mkruczkowski.randomcityapp.infrastructure.db.CityEntity
import com.mkruczkowski.randomcityapp.utils.mapList
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class CityRepositoryImpl @Inject constructor(
    private val cityDao: CityDao
) : CityRepository {

    private val selectedCityState = MutableStateFlow<City?>(null)

    override fun setSelectedCity(city: City?) {
        selectedCityState.value = city
    }

    override fun getSelectedCity() = selectedCityState.asStateFlow()

    override suspend fun addCity(city: City) {
        cityDao.insertCity(
            CityEntity(
                name = city.name,
                color = city.color,
                timestamp = city.timestamp
            )
        )
    }

    override fun getCities() =
        cityDao.getCities().mapList { city -> City(city.name, city.color, city.timestamp) }

}