package com.mkruczkowski.randomcityapp.utils

import android.content.Context
import android.graphics.Color
import android.widget.TextView
import androidx.databinding.BindingAdapter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

@BindingAdapter("textColorFromName")
fun TextView.setTextColor(colorName: String) {
    setTextColor(Color.parseColor(colorName))
}

fun <T, R> Flow<List<T>>.mapList(mapper: (T) -> R): Flow<List<R>> = map { list -> list.map { mapper(it) } }

fun Context.getBool(resId: Int) = resources.getBoolean(resId)