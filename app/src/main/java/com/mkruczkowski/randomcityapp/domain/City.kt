package com.mkruczkowski.randomcityapp.domain

data class City(val name: String, val color: String, val timestamp: String)