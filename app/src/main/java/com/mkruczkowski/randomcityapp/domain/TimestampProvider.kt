package com.mkruczkowski.randomcityapp.domain

interface TimestampProvider {
    fun getUTCNow(): String
}