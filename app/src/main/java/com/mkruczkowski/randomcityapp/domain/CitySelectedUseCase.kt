package com.mkruczkowski.randomcityapp.domain

import javax.inject.Inject

class CitySelectedUseCase @Inject constructor(private val cityRepository: CityRepository) {

    fun execute(city: City) {
        cityRepository.setSelectedCity(city)
    }

}