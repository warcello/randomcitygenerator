package com.mkruczkowski.randomcityapp.domain

interface ForegroundTask {
    fun start()
    fun stop()
}