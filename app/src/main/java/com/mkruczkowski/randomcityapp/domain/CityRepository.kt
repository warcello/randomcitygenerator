package com.mkruczkowski.randomcityapp.domain

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface CityRepository {

    fun setSelectedCity(city: City?)
    fun getSelectedCity(): StateFlow<City?>

    suspend fun addCity(city: City)
    fun getCities(): Flow<List<City>>

}