package com.mkruczkowski.randomcityapp.domain

import com.mkruczkowski.randomcityapp.di.IoDispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import javax.inject.Inject

class RandomCityGenerator @Inject constructor(
    private val cityProvider: RandomCityProvider,
    private val cityRepository: CityRepository,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher,
) : ForegroundTask {

    companion object {
        const val GENERATION_INTERVAL = 5000L
    }

    private val coroutineScope = CoroutineScope(ioDispatcher + SupervisorJob())
    private var generatorJob: Job = Job()

    override fun start() {
        generatorJob = startGeneratingCities()
    }

    override fun stop() {
        generatorJob.cancel()
    }

    private fun startGeneratingCities() = coroutineScope.launch {
        while (isActive) {
            delay(GENERATION_INTERVAL)
            cityRepository.addCity(cityProvider.getRandomCity())
        }
    }

}