package com.mkruczkowski.randomcityapp.domain

import javax.inject.Inject

class RandomCityProvider @Inject constructor(
    private val timestampProvider: TimestampProvider
) {

    private val cities = listOf("Gdańsk", "Warszawa", "Poznań", "Białystok", "Wrocław", "Katowice", "Kraków")
    private val colors = listOf("Yellow", "Green", "Blue", "Red", "Black", "White")

    fun getRandomCity() = City(cities.random(), colors.random(), timestampProvider.getUTCNow())

}