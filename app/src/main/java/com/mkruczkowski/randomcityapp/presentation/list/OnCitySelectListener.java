package com.mkruczkowski.randomcityapp.presentation.list;

import com.mkruczkowski.randomcityapp.domain.City;

interface OnCitySelectListener {
    void onCitySelected(final City city);
}