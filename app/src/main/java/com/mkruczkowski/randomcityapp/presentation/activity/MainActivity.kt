package com.mkruczkowski.randomcityapp.presentation.activity

import android.graphics.Color.parseColor
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.lifecycleScope
import com.mkruczkowski.randomcityapp.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val viewModel: MainActivityViewModel by viewModels()
    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        lifecycleScope.launch {
            viewModel.toolbarState.collect(::setCityOnToolbar)
        }
    }

    private fun setCityOnToolbar(toolbarData: ToolbarData?) = toolbarData?.let {
        toolbar.visibility = VISIBLE
        title = it.title
        toolbar.setBackgroundColor(parseColor(it.color))
    } ?: run {
        toolbar.visibility = GONE
    }
}