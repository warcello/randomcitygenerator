package com.mkruczkowski.randomcityapp.presentation.splash

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.mkruczkowski.randomcityapp.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashFragment : Fragment(R.layout.splash_fragment) {

    private val viewModel: SplashViewModel by viewModels()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        lifecycleScope.launch {
            viewModel.anyCityGenerationState.collect { isAnyCityGenerated ->
                if (isAnyCityGenerated) navigateToMasterFragment()
            }
        }
    }

    private fun navigateToMasterFragment() {
        findNavController().navigate(R.id.action_splashFragment_to_masterFragment)
    }

}