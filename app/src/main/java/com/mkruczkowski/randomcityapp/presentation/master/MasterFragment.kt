package com.mkruczkowski.randomcityapp.presentation.master

import androidx.fragment.app.Fragment
import com.mkruczkowski.randomcityapp.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MasterFragment : Fragment(R.layout.master_fragment)