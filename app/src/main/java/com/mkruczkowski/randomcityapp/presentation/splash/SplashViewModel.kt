package com.mkruczkowski.randomcityapp.presentation.splash

import androidx.lifecycle.ViewModel
import com.mkruczkowski.randomcityapp.domain.CityRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class SplashViewModel @Inject constructor(
    cityRepository: CityRepository,
) : ViewModel() {

    val anyCityGenerationState = cityRepository.getCities().map { it.isNotEmpty() }.distinctUntilChanged()

}