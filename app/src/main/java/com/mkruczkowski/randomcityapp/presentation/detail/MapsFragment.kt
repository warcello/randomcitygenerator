package com.mkruczkowski.randomcityapp.presentation.detail

import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.mkruczkowski.randomcityapp.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

private const val CITY_ZOOM = 10.0f

@AndroidEntryPoint
class MapsFragment : Fragment(R.layout.maps_fragment) {

    private val viewModel: DetailViewModel by viewModels()
    private val showCityCallback = OnMapReadyCallback { map ->
        lifecycleScope.launch {
            viewModel.selectedCityFlow.collect { city ->
                city?.let { map.showCity(it.name) }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(showCityCallback)
    }

    private fun GoogleMap.showCity(cityName: String) = runCatching {
        Geocoder(context).getFromLocationName(cityName, 1)
            .firstOrNull()?.run {
                val pos = LatLng(latitude, longitude)
                addMarker(MarkerOptions().position(pos).title(cityName))
                moveCamera(CameraUpdateFactory.newLatLngZoom(pos, CITY_ZOOM))
            }
    }.onFailure {
        Log.e("RandomCityApp", "Something went wrong, cannot show the city!")
    }.getOrNull()

}