package com.mkruczkowski.randomcityapp.presentation.list

import androidx.recyclerview.widget.DiffUtil
import com.mkruczkowski.randomcityapp.domain.City

class CityDiffCallback : DiffUtil.ItemCallback<City>() {

    override fun areItemsTheSame(oldItem: City, newItem: City) =
        oldItem == newItem

    override fun areContentsTheSame(oldItem: City, newItem: City) =
        oldItem == newItem

}