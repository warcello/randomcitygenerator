package com.mkruczkowski.randomcityapp.presentation.list

import androidx.lifecycle.ViewModel
import com.mkruczkowski.randomcityapp.domain.City
import com.mkruczkowski.randomcityapp.domain.CityRepository
import com.mkruczkowski.randomcityapp.domain.CitySelectedUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(
    cityRepository: CityRepository,
    private val citySelectedUseCase: CitySelectedUseCase,
) : ViewModel() {

    val cityListState = cityRepository.getCities().map { it.sortedBy(City::name) }

    fun citySelected(city: City) {
        citySelectedUseCase.execute(city)
    }

}