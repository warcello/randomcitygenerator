package com.mkruczkowski.randomcityapp.presentation.activity

data class ToolbarData(val title: String, val color: String)