package com.mkruczkowski.randomcityapp.presentation.list;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.mkruczkowski.randomcityapp.databinding.ListItemCityBinding;
import com.mkruczkowski.randomcityapp.domain.City;

import static com.mkruczkowski.randomcityapp.databinding.ListItemCityBinding.inflate;

public class CityListAdapter extends ListAdapter<City, CityListAdapter.ViewHolder> {

    final OnCitySelectListener citySelectListener;

    public CityListAdapter(final OnCitySelectListener listener) {
        super(new CityDiffCallback());
        citySelectListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ViewHolder.from(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final City city = getItem(position);
        if (city == null) return;

        holder.bind(city);
        holder.itemView.setOnClickListener(v -> citySelectListener.onCitySelected(city));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ListItemCityBinding binding;

        public ViewHolder(final ListItemCityBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(final City city) {
            binding.setCity(city);
            binding.executePendingBindings();
        }

        public static ViewHolder from(final ViewGroup parent) {
            final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            final ListItemCityBinding binding = inflate(inflater, parent, false);
            return new ViewHolder(binding);
        }
    }
}