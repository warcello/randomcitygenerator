package com.mkruczkowski.randomcityapp.presentation.detail

import androidx.lifecycle.ViewModel
import com.mkruczkowski.randomcityapp.domain.CityRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(
    cityRepository: CityRepository
) : ViewModel() {
    val selectedCityFlow = cityRepository.getSelectedCity()
}