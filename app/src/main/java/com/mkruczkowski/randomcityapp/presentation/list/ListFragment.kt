package com.mkruczkowski.randomcityapp.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.inflate
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.mkruczkowski.randomcityapp.R
import com.mkruczkowski.randomcityapp.databinding.ListFragmentBinding
import com.mkruczkowski.randomcityapp.domain.City
import com.mkruczkowski.randomcityapp.presentation.activity.MainActivityViewModel
import com.mkruczkowski.randomcityapp.utils.getBool
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
open class ListFragment : Fragment() {

    private val activityViewModel: MainActivityViewModel by activityViewModels()
    private val viewModel: ListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        activityViewModel.clearToolbarState()
        val binding: ListFragmentBinding = inflate(inflater, R.layout.list_fragment, container, false)
        binding.cityList.adapter = createAdapter()
        binding.lifecycleOwner = this
        return binding.root
    }

    private fun createAdapter() =
        CityListAdapter(::onCitySelected).also { adapter ->
            viewLifecycleOwner.lifecycleScope.launch {
                viewModel.cityListState.collect {
                    adapter.submitList(it)
                }
            }
        }

    private fun onCitySelected(city: City) {
        viewModel.citySelected(city)
        navigateToNextFragmentIfNeeded()
    }

    private fun navigateToNextFragmentIfNeeded() {
        if (!isTabletInLandscape()) {
            findNavController().navigate(R.id.action_masterFragment_to_mapsFragment)
        }
    }

    private fun isTabletInLandscape() = context?.getBool(R.bool.isTabletInLandscape) ?: false

}