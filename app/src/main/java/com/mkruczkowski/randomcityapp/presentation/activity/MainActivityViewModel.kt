package com.mkruczkowski.randomcityapp.presentation.activity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mkruczkowski.randomcityapp.domain.CityRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(cityRepository: CityRepository) : ViewModel() {

    private val mutableToolbarState = MutableStateFlow<ToolbarData?>(null)
    val toolbarState = mutableToolbarState.asStateFlow()

    init {
        viewModelScope.launch {
            cityRepository.getSelectedCity().collect { city ->
                mutableToolbarState.value = city?.run { ToolbarData(name, color) }
            }
        }
    }

    fun clearToolbarState() {
        mutableToolbarState.value = null
    }

}