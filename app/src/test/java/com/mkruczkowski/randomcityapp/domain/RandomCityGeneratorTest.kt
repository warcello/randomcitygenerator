package com.mkruczkowski.randomcityapp.domain

import com.mkruczkowski.randomcityapp.domain.RandomCityGenerator.Companion.GENERATION_INTERVAL
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
private class RandomCityGeneratorTest {

    private val testCity = City("name", "color", "timestamp")

    private val cityProvider = mockk<RandomCityProvider> {
        every { getRandomCity() } returns testCity
    }
    private val cityRepository = mockk<CityRepository> {
        coJustRun { addCity(any()) }
    }

    private val testDispatcher = TestCoroutineDispatcher()
    private val generator = RandomCityGenerator(cityProvider, cityRepository, testDispatcher)

    @BeforeEach
    fun setup() {
        Dispatchers.setMain(testDispatcher)
    }

    @AfterEach
    fun cleanUp() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    @Test
    fun `With started generator, when generation interval time is passed, new city is added to the repository`() =
        testDispatcher.runBlockingTest {
            //given
            generator.start()

            //when
            advanceTimeBy(GENERATION_INTERVAL)

            //then
            generator.stop()
            coVerify(exactly = 1) { cityRepository.addCity(testCity) }
        }

    @Test
    fun `With started generator, when generation interval time did not pass and generator is stopped, new city is not added to the repository`() =
        testDispatcher.runBlockingTest {
            //given
            generator.start()
            advanceTimeBy(GENERATION_INTERVAL / 2)

            //when
            generator.stop()
            advanceTimeBy(GENERATION_INTERVAL / 2)

            //then
            coVerify(exactly = 0) { cityRepository.addCity(testCity) }
        }

}