package com.mkruczkowski.randomcityapp.domain

import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

private class CitySelectedUseCaseTest {

    private val cityRepository = mockk<CityRepository>(relaxUnitFun = true)
    private val useCase = CitySelectedUseCase(cityRepository)

    @Test
    fun `With a city, when use case is executed with it, this city should be set as selected`() {

        //given
        val anyCity = City("name", "color", "timestamp")

        //when
        useCase.execute(anyCity)

        //then
        verify(exactly = 1) { cityRepository.setSelectedCity(anyCity) }

    }

}